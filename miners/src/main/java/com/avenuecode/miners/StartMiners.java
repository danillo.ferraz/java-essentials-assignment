package com.avenuecode.miners;

import com.avenuecode.blockchain.Blockchain;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Slf4j
public class StartMiners {



    public static void main(String[] args) {

        /* User Input Version */
        var scanner = new Scanner(System.in);
        log.info("INSERT THE BLOCKCHAIN MAX_SIZE (INT)");
        var blockchainMaxSize = scanner.nextInt();
        log.info("INSERT THE BLOCKCHAIN DIFFICULTY TO MINE A BLOCK (INT)");
        var difficulty = scanner.nextInt();
        log.info("INSERT THE NUMBER OF MINERS (INT)");
        var numberOfMiners = scanner.nextInt();


        /*single instance of blockchain - miners will compete for this resource */
        var blockchain = new Blockchain(difficulty, blockchainMaxSize);

        log.info("Application parameters: "
                +" Number of miners = "+ numberOfMiners
                + " Maximum blockchain size = "+ blockchainMaxSize
                + " Difficulty = " + difficulty);
        log.info("Blockchain size: " + blockchain.getBlockchain().size());

        /* create the miners and put them into a list with the reference of the blockchain */
        List<Miner> miners = new ArrayList<>();
        for (var i = 0; i < numberOfMiners; i++) {
            miners.add(new Miner(blockchain, "Miner " + letter(i)));
        }

        /* the mining starts here and only stops when the blockchain reaches its size limit*/
        var executorService = Executors.newFixedThreadPool(miners.size());
        List<Future<?>> listOfFuture = miners.stream().map(
                miner -> executorService.submit(miner::mine)
        ).collect(Collectors.toList());

        /* main thread awaits for the threads to finish so we can log the aftermath below */
        listOfFuture.stream().forEach(f -> {
            try {
                f.get();
            } catch (InterruptedException | ExecutionException e) {
                log.error(e.getMessage());
            }
        });
        executorService.shutdown();

        log.info("Final blockchain size is " + blockchain.getBlockchain().size());
        log.info("Is it a valid blockchain? " + blockchain.isChainValid());
        log.info("Application finished.");

    }

    /**
     * Returns corresponding letter of the alphabet giving the number of the input
     */
    public static char letter(int i) {
        return (char) ('A' + (i%26));
    }


}
