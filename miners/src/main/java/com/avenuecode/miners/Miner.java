package com.avenuecode.miners;

import com.avenuecode.blockchain.Block;
import com.avenuecode.blockchain.Blockchain;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Miner {

    private Blockchain blockchain;
    private Block block;
    private String name;

    public Miner(Blockchain blockchain, String name) {
        this.blockchain = blockchain;
        this.name = name;
    }

    public void feed(Block b) {
        this.block = b;
    }

    public void mine() {
        while (blockchain.hasReachedLimit()) {
            var blocksToMine = blockchain.getAmountOfBlocksLeft();
            log.info(this.name + ": "+blocksToMine+" blocks remaining to be mined.");
            if(blockchain.isEmptyBlockchain()){
                feed(Block.createGenesisBlock());
                var result = this.block.mineBlock(blockchain.getDifficulty(), name);
                blockchain.addNewBlock(result);
            }else{
                feed(new Block("new block!", blockchain.getLastBlock().getHash()));
                var result = this.block.mineBlock(blockchain.getDifficulty(), name);
                blockchain.addNewBlock(result);
            }
        }
        log.info(this.name + ": finished mining!");
    }

}
