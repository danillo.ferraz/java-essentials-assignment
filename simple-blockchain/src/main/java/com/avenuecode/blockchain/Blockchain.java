package com.avenuecode.blockchain;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class Blockchain {

    private List<Block> blockchain;
    private int difficulty;
    private int maxSize;

    public Blockchain(int difficulty, int maxSize) {
        this.difficulty = difficulty;
        this.blockchain = new ArrayList<>();
        this.maxSize = maxSize;
    }

    public synchronized boolean addNewBlock(Block newBlock) {

        if (isGenesisBlock(newBlock)) {
            blockchain.add(newBlock);
            log.info(newBlock.getMinedBy() + ": added to the blockchain.");
            return true;
        } else if (isValidPreviousHash(newBlock)) {
            this.blockchain.add(newBlock);
            log.info(newBlock.getMinedBy() + ": added to the blockchain.");
            return true;
        } else {
            log.info(newBlock.getMinedBy() + ": was too late and couldn't add to the blockchain.");
            return false;
        }
    }

    private boolean isGenesisBlock(Block newBlock) {
        return blockchain.isEmpty() && newBlock.getPreviousHash().equals("0");
    }

    public boolean isValidPreviousHash(Block newBlock) {
        if (blockchain.size() == 0) { return false; };
        return newBlock.getPreviousHash() == this.getLastBlock().getHash();
    }

    public int getDifficulty() {
        return this.difficulty;
    }

    public List<Block> getBlockchain() {
        return this.blockchain;
    }

    public Block getLastBlock() {
        return this.blockchain.get(this.getBlockchain().size()-1);
    }

    public Boolean isChainValid() {
        Block currentBlock;
        Block previousBlock;
        var hashTarget = new String(new char[difficulty]).replace('\0', '0');

        //loop through blockchain to check hashes:
        for (int i = 1; i < blockchain.size(); i++) {
            currentBlock = blockchain.get(i);
            previousBlock = blockchain.get(i - 1);
            //compare registered hash and calculated hash:
            if (!currentBlock.getHash().equals(currentBlock.calculateHash())) {
                log.info("Current Hashes not equal.");
                return false;
            }
            //compare previous hash and registered previous hash
            if (!previousBlock.getHash().equals(currentBlock.getPreviousHash())) {
                log.info("Previous Hashes not equal");
                return false;
            }
            //check if hash is solved
            if (!currentBlock.getHash().substring(0, difficulty).equals(hashTarget)) {
                log.info("This block hasn't been mined");
                return false;
            }
        }
        return true;
    }

    public Boolean isEmptyBlockchain() {
        return this.blockchain.isEmpty();
    }

    public int getBlockchainSize(){
        return this.blockchain.size();
    }

    public int getBlockchainMaxSize() { return this.maxSize; }

    public boolean hasReachedLimit() {
        return this.getBlockchainSize() < this.getBlockchainMaxSize();
    }

    public int getAmountOfBlocksLeft() {
        return this.getBlockchainMaxSize() - this.getBlockchainSize();
    }
}
