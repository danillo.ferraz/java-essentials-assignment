package com.avenuecode.blockchain;

import lombok.extern.slf4j.Slf4j;

import java.util.Date;

@Slf4j
public class Block {

    private String hash;
    private String previousHash;
    private String data;
    private long timeStamp;
    private int nonce;
    private String minedBy;

    public Block(String data,String previousHash ) {
        this.data = data;
        this.previousHash = previousHash;
        this.timeStamp = new Date().getTime();
        this.hash = calculateHash(); //Making sure we do this after we set the other values.
    }

    public String calculateHash() {
        return StringUtil.applySha256(
                previousHash +
                        Long.toString(timeStamp) +
                        Integer.toString(nonce) +
                        data
        );
    }

    public static Block createGenesisBlock() {
        return new Block("Genesis Block", "0");
    }

    public Block mineBlock(int difficulty, String minerName) {
        String target = new String(
                new char[difficulty]).replace(
                        '\0', '0'); //Create a string with difficulty * "0"
        while(!hash.substring( 0, difficulty).equals(target)) {
            nonce ++;
            hash = calculateHash();
        }
        log.info(minerName + ": block mined with hash "+ this.hash);
        this.minedBy = minerName;
        return this;
    }

    public String getMinedBy() {
        if(this.minedBy == null) return "NOT_MINED";
        return this.minedBy;
    }

    public String getHash() {
        return this.hash;
    }

    public String getPreviousHash() {
        return this.previousHash;
    }

}
