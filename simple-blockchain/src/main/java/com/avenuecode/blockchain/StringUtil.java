package com.avenuecode.blockchain;

import java.security.MessageDigest;

public class StringUtil {

    private StringUtil() {
        throw new RuntimeException("Utility class");
    }

    //Applies Sha256 to a string and returns the result.
    public static String applySha256(String input){
        try {
            var digest = MessageDigest.getInstance("SHA-256");
            //Applies sha256 to our input,
            var hash = digest.digest(input.getBytes("UTF-8"));
            var hexString = new StringBuffer(); // This will contain hash as hexidecimal
            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        }
        catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}
