# ACP2P Java Essentials
## Week 1 and 2 - Modern Java Assignment
### Blockchain + Miners project

This is a multi-module maven project with two modules:
```simple-blockchain``` and ```miners```

The project is a console application that receives user's inputs for:

```BLOCKCHAIN MAX_SIZE``` -  which is the maximum size of the blockchain, once this is a project for concurrency/threads learning purposes, the blockchain will be a shared resource among the Miner's threads, so this is the stop condition for them.

```DIFFICULTY``` -  is a common parameter for blockchains and represents how hard is going to be for the miners to "solve" a mathematical problem in order to be able to add a block into the blockchain. In this project the difficulty represents the number of zeros is needeed at the beggining of the block's hash after calculating it over with a certain **NONCE**

```NUMBER OF MINERS``` - each miner will be running in an independent thread and will compete to add blocks into the blockchain


Also this project has the objective to highlight some features like:

- Local Variable Type Inference.
- Lambda Expressions along with the Streams API.
- Features of the Collections API.

